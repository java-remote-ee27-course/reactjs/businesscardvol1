import "./App.css";
import "./index.css";
import { Emoji } from "emoji-picker-react";

function App() {
  const cardText1 = `Sed ut perspiciatis unde omnis iste natus error sit
                    voluptatem accusantium doloremque laudantium,
                    totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                    et quasi architecto beatae vitae dicta sunt explicabo. Photo: Pexels`;
  const cardText2 = `Nam libero tempore, cum soluta nobis est eligendi optio, 
                    cumque nihil impedit, quo minus id, 
                    quod maxime placeat facere possimus, 
                    omnis voluptas assumenda est, 
                    omnis dolor repellendus. Photo: Pexels`;

  const tags = [
    {
      backgroundColor: "green",
      color: "white",
      text: "ballet",
      type: "classical",
    },
    {
      backgroundColor: "yellow",
      color: "black",
      text: "salsa",
      type: "latin",
    },
    {
      backgroundColor: "blue",
      color: "white",
      text: "hiphop",
      type: "contemporary",
    },
    {
      backgroundColor: "red",
      color: "white",
      text: "tango",
      type: "latin",
    },
    {
      backgroundColor: "orange",
      color: "black",
      text: "street",
      type: "contemporary",
    },
  ];

  return (
    <div className="App">
      <header className="App-header"></header>
      <BusinessCard
        photo="assets/pexels-koolshooters.jpg"
        photoName="dancer"
        headerText="Thomas Nelson"
        text={cardText1}
        tags={[tags[0], tags[1], tags[3]]}
      />

      <BusinessCard
        photo="assets/pexels-budgeron-bach.jpg"
        photoName="dancer"
        headerText="Liza Louise"
        text={cardText2}
        tags={[tags[0], tags[2], tags[3], tags[4]]}
      />
    </div>
  );
}

export default App;

function BusinessCard(props) {
  return (
    <div className="business-card">
      <CardPicture photo={props.photo} photoName={props.photoName} />
      <CardHeader header={props.headerText} />
      <CardMain text={props.text} />
      <CardFooter footerTags={props.tags} />
    </div>
  );
}

function CardPicture({ photo, photoName }) {
  return (
    <div className="card-picture">
      <img src={photo} alt={photoName} />
    </div>
  );
}

function CardHeader({ header }) {
  return (
    <div className="card-header">
      <h1>{header}</h1>
    </div>
  );
}

function CardMain({ text }) {
  return <div className="card-main">{text}</div>;
}

function CardFooter({ footerTags }) {
  const tagged = footerTags.map((tag) => (
    <Tag key={tag.text} tagObject={tag} />
  ));

  return <div className="card-footer">{tagged}</div>;
}

function Tag({ tagObject }) {
  const emojiBallet = <Emoji unified="1f57a" size="15" />;
  const emojiLatin = <Emoji unified="1f483" size="15" />;
  const emojiHiphop = <Emoji unified="1f938" size="15" />;

  return (
    <div
      className="tag"
      style={{
        backgroundColor: tagObject.backgroundColor,
        color: tagObject.color,
      }}
    >
      {tagObject.text} &nbsp;
      {tagObject.type === "classical" && emojiBallet}
      {tagObject.type === "latin" && emojiLatin}
      {tagObject.type === "contemporary" && emojiHiphop}
    </div>
  );
}
