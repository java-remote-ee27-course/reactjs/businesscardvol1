# Reusable Business Card Demo

May 2024

A small demo app created in ReactJS. Re-usable business card with re-usable components. The demo shows 2 business cards created by using the same components.

## Screenshots

![Business-cards-demo](./public/assets/demopic2.png)

## Author

Katlin Kalde

- Photos: Pexels, Budgeron Bach, Koolshooters

## Rights

Free to use as you like, no attribution needed. For the photo rights see the Pexels https://pexels.com licence policy.
